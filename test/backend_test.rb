# frozen_string_literal: true

require "minitest/autorun"
require "tmpdir"

class BackendTest < Minitest::Test # rubocop:disable Metrics/ClassLength
  WORKDIR = Dir.pwd
  REPOS_PATH = File.expand_path("./fixtures", __dir__)

  format = %w[
    <prefix>
    <branch>
    <behind>
    <ahead>
    <separator>
    <index>
    <unmerged>
    <worktree>
    <untracked>
    <clean>
    <suffix>
  ].join

  ARGS = {
    "--format"    => format,
    "--reset"     => "∅",
    "--prefix"    => "(",
    "--suffix"    => ")",
    "--branch"    => "≡",
    "--behind"    => "↓",
    "--ahead"     => "↑",
    "--separator" => "|",
    "--index"     => "●",
    "--unmerged"  => "✖",
    "--worktree"  => "✚",
    "--untracked" => "∗",
    "--clean"     => "✔"
  }.flatten.freeze

  def self.backend_cmd
    return @backend_cmd if defined?(@backend_cmd)

    cmd = ARGV.empty? ? ["ruby/ugp.rb"] : ARGV
    cmd[0] = File.expand_path(cmd[0], WORKDIR) if cmd[0].include?("/")

    @backend_cmd = cmd.freeze
  end

  def system!(*cmd)
    system(*cmd, exception: true)
  end

  def chdir_repo(name, &block)
    Dir.chdir(File.join(REPOS_PATH, name)) do |fixture_dir|
      system!("../configure", "--setup")

      begin
        Dir.chdir(File.join(fixture_dir, "local"), &block)
      ensure
        system!("../configure", "--teardown")
      end
    end
  end

  def ugp(*args)
    cmd = self.class.backend_cmd.dup.concat(ARGS, args)

    out, err = capture_subprocess_io do
      system(*cmd)
    end

    [$?.dup, out, err]
  end

  def test_00
    Dir.mktmpdir do |dir|
      Dir.chdir(dir) do
        s, out, err = ugp
        assert_equal("", err)
        assert_equal("\n", out)
        assert_equal(0, s.exitstatus)
      end
    end
  end

  def test_01
    chdir_repo("01") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅↑1∅|∅✚1∅∗2∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_02
    chdir_repo("02") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅↑3∅|∅✚1∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_03
    chdir_repo("03") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅|∅●1∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_04
    chdir_repo("04") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅↑1∅|∅✔∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_05
    chdir_repo("05") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅|∅●3∅✖2∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_06
    rgx = /\(∅≡[a-z0-9]{7}∅\|∅✔∅\)∅\n\z/

    chdir_repo("06") do
      s, out, err = ugp
      assert_equal("", err)
      assert(out.match?(rgx), "Expected #{out.inspect} to match #{rgx.inspect}")
      assert_equal(0, s.exitstatus)
    end
  end

  def test_07
    chdir_repo("07") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅|∅●1∅✚2∅∗1∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_08
    chdir_repo("08") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅↓5∅↑4∅|∅●1∅✚1∅∗1∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_09
    chdir_repo("09") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅↓5∅↑4∅|∅●1∅✚1∅∗1∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_09_a
    chdir_repo("09") do
      s, out, err = ugp("--format", "The repo is <clean>")
      assert_equal("", err)
      assert_equal("The repo is \n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_10
    chdir_repo("10") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅|∅✔∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  # unable to reproduce test 11 with an actual repo.

  def test_12
    chdir_repo("12") do
      s, out, err = ugp
      assert_equal("", err)
      assert_equal("(∅≡master∅|∅✔∅)∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  def test_12_a
    chdir_repo("12") do
      s, out, err = ugp("--format", "<suffix><separator><branch><separator><prefix>", "--branch")
      assert_equal("", err)
      assert_equal(")∅|∅master∅|∅(∅\n", out)
      assert_equal(0, s.exitstatus)
    end
  end

  # TODO: add a test where a Git command fails
end
