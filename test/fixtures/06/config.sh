function setup {
  mkdir local

  pushd local
    git init
    git commit --allow-empty --message commit_foo

    echo foo > file0
    git add file0
    git commit --message commit_foo

    git checkout --quiet HEAD~
  popd
}

function teardown {
  rm -rf local
}
