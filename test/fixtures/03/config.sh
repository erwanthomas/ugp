function setup {
  mkdir local

  pushd local
    git init
    echo foo > file0
    git add file0
    git commit --message commit_foo
    echo bar > file0
    git add file0
  popd
}

function teardown {
  rm -rf local
}
