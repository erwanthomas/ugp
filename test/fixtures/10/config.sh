function setup {
  mkdir local

  pushd local
    git init
  popd
}

function teardown {
  rm -rf local
}
