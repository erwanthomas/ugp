function setup {
  mkdir local

  pushd local
    git init

    touch file0 file1 file2 file3
    git add file0 file1 file2
    git commit --message commit_foo

    rm file0 file1 file2
    git add file0
  popd
}

function teardown {
  rm -rf local
}
