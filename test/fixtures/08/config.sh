function setup {
  mkdir local remote

  pushd remote
    git init --bare
  popd

  pushd local
    git init
    git remote add origin ../remote
    git commit --allow-empty --message commit_foo

    for i in $(seq 1 5)
    do
      echo "foo$i\n" > file0
      git add file0
      git commit --message commit_foo
    done

    git push --quiet --set-upstream origin master

    git reset --hard origin/master~5

    for i in $(seq 1 4)
    do
      echo "bar$i\n" >> file0
      git add file0
      git commit --message commit_foo
    done

    echo '' > file0
    touch file{1,2}
    git add file1
  popd
}

function teardown {
  rm -rf local remote
}
