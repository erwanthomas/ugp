prompt off
autoload -U colors
colors
setopt PROMPT_SUBST

UGP_PATH="${0:A:h}"

UGP_FORMAT="<prefix><branch><behind><ahead><separator><index><unmerged><worktree><untracked><clean><suffix>"
UGP_RESET="%{${reset_color}%}"
UGP_PREFIX="("
UGP_SUFFIX=")"
UGP_BRANCH="%{$fg_bold[magenta]%}"
UGP_BEHIND="%{↓%G%}"
UGP_AHEAD="%{↑%G%}"
UGP_SEPARATOR="|"
UGP_INDEX="%{$fg[red]%}%{●%G%}"
UGP_UNMERGED="%{$fg[red]%}%{✖%G%}"
UGP_WORKTREE="%{$fg[blue]%}%{✚%G%}"
UGP_UNTRACKED="%{$fg_bold[yellow]%}%{∗%G%}"
UGP_CLEAN="%{$fg_bold[green]%}%{✔%G%}"

ugp_display () {
  [ -z "$UGP_EXECUTABLE" ] && UGP_EXECUTABLE="$UGP_PATH/ruby/ugp.rb"

  echo $("$UGP_EXECUTABLE" \
    --format "$UGP_FORMAT" \
    --reset "$UGP_RESET" \
    --prefix "$UGP_PREFIX" \
    --suffix "$UGP_SUFFIX" \
    --branch "$UGP_BRANCH" \
    --behind "$UGP_BEHIND" \
    --ahead "$UGP_AHEAD" \
    --separator "$UGP_SEPARATOR" \
    --index "$UGP_INDEX" \
    --unmerged "$UGP_UNMERGED" \
    --worktree "$UGP_WORKTREE" \
    --untracked "$UGP_UNTRACKED" \
    --clean "$UGP_CLEAN")
}
