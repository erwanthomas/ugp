package parser

import (
	"bytes"
	"strconv"
)

type Result struct {
	Oid       string
	Head      string
	Branch    string
	Ahead     int
	Behind    int
	Index     int
	Worktree  int
	Unmerged  int
	Untracked int
}

func (r *Result) GetBranch() string {
	switch {
	case r.Branch != "":
		return r.Branch
	case r.Head != "":
		return r.Head
	case r.Oid != "":
		return r.Oid[0:7]
	default:
		return ""
	}
}

func (r *Result) IsClean() bool {
	return r.Index == 0 && r.Unmerged == 0 && r.Worktree == 0 && r.Untracked == 0
}

var prefOid = []byte("# branch.oid")
var prefHead = []byte("# branch.head")
var prefAb = []byte("# branch.ab")
var prefXY1 = []byte("1")
var prefXY2 = []byte("2")
var prefUnmerged = []byte("u")
var prefUntracked = []byte("?")
var point = byte('.')

type Parser struct {
	Res *Result
}

func New() Parser {
	return Parser{&Result{}}
}

func (p Parser) WriteV2(l []byte) error {
	r := p.Res

	switch {
	case bytes.HasPrefix(l, prefXY1) || bytes.HasPrefix(l, prefXY2):
		if l[2] != point {
			r.Index++
		}
		if l[3] != point {
			r.Worktree++
		}

	case bytes.HasPrefix(l, prefUntracked):
		r.Untracked++

	case bytes.HasPrefix(l, prefUnmerged):
		r.Unmerged++

	case bytes.HasPrefix(l, prefOid):
		oid := string(l[13:len(l)])

		if oid != "(initial)" {
			r.Oid = oid
		}

	case bytes.HasPrefix(l, prefHead):
		head := string(l[14:len(l)])

		if head != "(detached)" {
			r.Head = head
		}

	case bytes.HasPrefix(l, prefAb):
		ab := bytes.Split(l[12:len(l)], []byte(" "))
		a := ab[0]
		b := ab[1]

		if ai, err := strconv.Atoi(string(a[1:len(a)])); err == nil {
			r.Ahead += ai
		}

		if bi, err := strconv.Atoi(string(b[1:len(b)])); err == nil {
			r.Behind += bi
		}
	}

	return nil
}

func (p Parser) Result() *Result {
	return p.Res
}
